#pragma once
#include "CoronaAdapter.h"
#include "Utils.h"
#include <stdio.h>
#include <list>
#include <bitset>

class FileReceiver
{
public:
    FileReceiver();

    ~FileReceiver();

    /*
    *   Initialization Method, Parses Input And Creates All
    *   Of The Necesery Stuff For The Program
    * 
    *   @param argc     Number Of Arguments Given
    *   @param argv     Array Of The Arguments
    * 
    *   @return         True If Successful, False Otherwise
    * 
    */
    bool Init(int argc, char* argv[]);

    /*
    *   Main Program Loop
    */
    void Run();

private:

    /*
    *   Child Process' Loop For Printing The Progress
    */
    const void PrintLoop();

    /*
    *   Get The Start Connection Packet Containing
    *   The File Size And Name
    */
    void GetStart();

    /*
    *   Get Packets From The Network and Check Their Checksum and Type
    */
    const void ReceivePackets();

    /*
    *   Handle A Data Packet Received From The Network -
    *   Sort The Data In The m_readyToWrite List
    *   
    *   @param received     The Packet Received From The Network
    */
    void HandleMessage(Message& received);

    /*
    *   Write The Ready Packets To The File If Possible
    *
    *   @param lastWritten  The Seq Number Of The Last Written Packet
    *   
    *   @return             The Seq Number Of The Last Written Packet
    */
    size_t WriteToFile(uint32_t lastWritten);

    /*
    *   Send Ack Message To The Sender
    * 
    *   @param seq      Seq Number Of Ack
    *   @param opCode   Ack OpCode
    */
    const void SendAck(uint32_t seq, E_OpCode opCode);

private:

    FILE* m_file;
    uint32_t m_fileChunks;
    char m_fileName[FILENAME_LENGTH];
    uint16_t m_port;
    Coronet::Harmness m_harmness;
    Coronet::Coronet* m_coronet;
    CoronaAdapter m_adapter;
    std::list<Message> m_readyToWrite;
    std::bitset<MAX_FILE_PACKETS> m_readyIndex;
    int m_childPID;
    int m_pipeFDS[2];
    bool m_done;
};