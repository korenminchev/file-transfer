#pragma once
#include "CoronaAdapter.h"
#include "FileSender.h"
#include "Utils.h"
#include <arpa/inet.h>

class FileSenderManager
{
public:

    FileSenderManager();
    
    ~FileSenderManager();

    /*
    *   Initialization Method - Checks Input And Creates Child Processes
    * 
    *   @param argv     Array of Arguments
    *   @param argc     Number Of Arguments Given
    */
    bool Init(char* argv[], int argc);

    /*
    *   Main Run Function
    *   Responsible To Get The State Of Each Child And Print The Progress
    */
    void Run();

private:

    /*
    *   Parses And Checks The Input Given
    *   
    *   @param argv     Array of Arguments
    *   @param argc     Number Of Arguments Given  
    */
    bool ParseInput(char* argv[], int argc);

    /*
    *   Prints The Progress Of A Sender
    * 
    *   @param sender   The Sender To Print His Progress
    *   @param sent     Count Of Packets Successfully Sent By The Sender
    */
    const void PrintProgress(uint8_t sender, uint32_t sent);

private:

    Coronet::Harmness m_harmness;
    char m_filePath[FILENAME_LENGTH];
    sockaddr_in m_addresses[MAX_LISTENERS];
    int32_t m_pipeFDS[MAX_LISTENERS];
    FileSender m_fileSenders[MAX_LISTENERS];
    uint8_t m_numOfReceivers;
    int32_t m_epoll;
    int8_t m_senderNumber;
    uint32_t m_fileChunks;
};
