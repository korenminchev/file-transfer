#pragma once
#include "Constants.h"
#include "CoronaAdapter.h"
#include "Utils.h"

class Relay
{
public:
    Relay();
    ~Relay();

    /*
    *   Relay Initialization Method
    * 
    *   @param argc     Number Of Arguments Given
    *   @param argv     Array Of Arguments
    * 
    *   @return bool    True If Successfull, False Otherwise
    */
    bool Init(int argc, char* argv[]);

    /*
    *   Main Loop Transfering Packets Between A Sender And A Receiver
    */
    const void Run();
private:

    /*
    *   Parse And Check Input
    * 
    *   @param argc     Number Of Arguments Given
    *   @param argv     Array Of Arguments
    * 
    *   @return bool    True If Successfull, False Otherwise
    */
    bool ParseInput(int argc, char* argv[]);

    uint16_t m_sourcePort;
    sockaddr_in m_destAddr;
    Coronet::Harmness m_harmness;
    Coronet::Coronet* m_coronetSender;
    Coronet::Coronet* m_coronetReceiver;
    CoronaAdapter m_adapterSender;
    CoronaAdapter m_adapterReceiver;
};
