#pragma once
#if !defined(CORONET_LIBRARY)
#define CORONET_LIBRARY

#include <cstring>
#include <cstddef>
#include <cinttypes>
#include <unistd.h>
#include <cstdlib>
#include <ctime>

namespace Coronet
{
    enum class Harmness
    {
        Healthy = 0,                // Not affected from the virus at all [Aka TESTING MODE]
        IncubationPeriod = 1,       // You are not aware to the virus but there are little signs [Aka EASY MODE]
        MinorSymptoms = 2,          // Oh you are starting to pay attention to the virus [Aka MEDIUM MODE]
        SevereSymptoms = 3,          // Damn that coronavirus doing some hard shit [Aka HARD MODE]
        Dying = 4,                  // Say hello to your precious information [Aka EXTREME MODE]
    };

    class ITransmitter
    {
    
    public:

        /**
         * @brief Send data to an endpoint.
         * 
         * @param data              The data to send.
         * @param dataSize          The data size (in bytes).
         * 
         * @return ssize_t          The bytes amount that have been sent.
         *                          If it's an error return -ERRNO.
         */
        virtual ssize_t Send(const uint8_t* data, size_t dataSize) = 0;

    };

    class IReceiver
    {
    
    public:

        /**
         * @brief Receive data from an endpoint.
         * 
         * @param o_buffer          The buffer to receive into the data.
         * @param bufferSize        The buffer size.
         * 
         * @return ssize_t          The bytes amount that have been received.
         *                          If it's an error return -ERRNO.
         */
        virtual ssize_t Receive(uint8_t* o_buffer, size_t bufferSize) = 0;

    };

    class Coronet
    {

    public:

        /**
         * @brief Create a new Coronet.
         * 
         * @param transmitter       The transmitter we use to spread the virus.
         * @param receiver          The receiver we use to get the virus.
         * @param harmness          The harmness of the virus.
         */
        Coronet(ITransmitter& transmitter, IReceiver& receiver, Harmness harmness = Harmness::Healthy);

        /**
         * @brief Remove the default copy constructor, not needed.
         */
        Coronet(const Coronet& other) = delete;

        /**
         * @brief Send some data to your target, with slightly virus effects.
         * 
         * @param buffer        The buffer to send.
         * @param bufferSize    The buffer size. (in bytes)
         * 
         * @return ssize_t      The bytes amount that have been sent.
         *                      If it's an error return -ERRNO:
         *                      -EINVAL     In case of invalid length (bigger than MAX_DATA_SIZE)
         */
        ssize_t Send(uint8_t* buffer, size_t bufferSize);

        /**
         * @brief Receive some data from your target, with slightly virus effects.
         * 
         * @param o_buffer      The buffer to receive into the data.
         * @param bufferSize    The buffer size. (in bytes)
         * 
         * @return ssize_t      The bytes amount that have been received.
         *                      If it's an error return -ERRNO.
         */
        ssize_t Receive(uint8_t* o_buffer, size_t bufferSize);

        /**
         * @brief Change the virus harmness.
         * 
         * @param harmness      The new harmness.
         */
        void ChangeHarmness(Harmness harmness);

        /**
         * The max data that you can send.
         * NOTE: Unlike clayworm, this library actually enforce the MTU.
         */
        static constexpr size_t MAX_DATA_SIZE = 1500;

    private:

        /**
         * The transmitter we use to spread the virus.
         */
        ITransmitter& m_transmitter;

        /**
         * The receiver we use to get the virus.
         */
        IReceiver& m_receiver;

        /**
         * The virus harmness.
         */
        Harmness m_harmness;

    };

}

#endif // CORONET_LIBRARY
