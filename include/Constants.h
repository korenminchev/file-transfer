#pragma once
#include <stdint.h>
#include "Messages.h"


constexpr uint16_t MESSAGE_SIZE = sizeof(DataMsg);
constexpr uint8_t MAX_LISTENERS = 10;
constexpr uint32_t MAX_FILE_SIZE = UINT32_MAX;
constexpr uint16_t WINDOW_SIZE = 1300;
constexpr uint8_t MAX_PENDING_PACKETS = 100;
constexpr uint8_t PORT_MAX_LEN = 5;
constexpr uint16_t PORT_MIN = 1024;
constexpr uint16_t TIMEOUT = 1;
constexpr int8_t ERROR = -1;
constexpr uint8_t VALIDATE_SEND = 20;
constexpr uint8_t PIPE_WRITE = 1;
constexpr uint8_t PIPE_READ = 0;
constexpr uint32_t MAX_FILE_PACKETS = MAX_FILE_SIZE/BUFFER_SIZE + 1;
constexpr uint16_t RELAY_TIMEOUT = 600;
constexpr ssize_t BAD_ADDR = -3;
constexpr ssize_t SOCK_TIMEOUT = -2;
constexpr uint8_t RECV_ARGS = 4;
constexpr uint8_t RELAY_ARGS = 6;
constexpr char CORONET_HEALTHY = 'H';
constexpr char CORONET_INCUBATION = 'I';
constexpr char CORONET_MINOR = 'M';
constexpr char CORONET_SEVERE = 'S';
constexpr char CORONET_DYING = 'D';