#pragma once
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include "Constants.h"

static uint16_t parsePort(const char* port)
{
    uint8_t size = 0;
    char digit;
    //start from index 6(where the actual port begins)
    //and check if each char is a digit and also count the size of the input
    while(size <= PORT_MAX_LEN && size < strlen(port))
    {
        digit = port[size];
        //check if char is digit
        if (isdigit(digit) == 0)
        {
            printf("Invalid Input, Port Can Be Digits Only\n");
            return 0;
        }
        size++;
    }

    //check if input is too large
    if(size > 5)
    {
        printf("Invalid Port Size(max port is 65,535)\n");
        return 0;
    }

    //get the port into a variable
    uint32_t finalPort = atoi(port);

    //check if port is larger than possible
    if(finalPort > UINT16_MAX || finalPort <= PORT_MIN)
    {
        printf("Invalid Port Size(1024 < port < 65535)\n");
        return 0;
    }

    return finalPort;
}

static bool InsertIntoEpoll(int32_t ePoll, int32_t fdToInsert)
{
    //Fill In Request To Insert FD Into Epoll
    struct epoll_event event;
    event.data.fd = fdToInsert;
    event.events = EPOLLIN;

    //Make The Request
    if (epoll_ctl(ePoll, EPOLL_CTL_ADD, fdToInsert, &event) == ERROR)
    {
        //If Failed
        printf("Error Inserting FD to Epoll\n");
        return false;
    }

    return true;
}

