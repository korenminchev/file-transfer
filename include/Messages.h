#pragma once
#include <Constants.h>
#include <netinet/in.h>


constexpr uint8_t FILENAME_LENGTH = 64;


enum E_MsgType
{
    Data = 0,
    Ack = 1,
    End = 2,
    Start = 3
};

enum E_OpCode
{
    Ok = 0,
    OkEnd,
    OkStart,
    Nok = 0xFFFFFFFF
};

struct __attribute__((__packed__)) SHeader
{
    E_MsgType type;
    uint32_t checkSum;
};

//To Create A Packet Of 1500 Bytes The Buffer Shall Be:
//(So We Could Send As Much Data As Possible Every Time)
constexpr uint16_t BUFFER_SIZE = 1500 - sizeof(SHeader) - sizeof(uint16_t) - sizeof(uint32_t);
//                                                        len variable       seq variable

struct __attribute__((__packed__)) DataMsg
{
    SHeader header;
    uint16_t len;
    uint32_t seq;
    uint8_t data[BUFFER_SIZE];
};

struct __attribute__((__packed__)) AckMsg
{
    SHeader header;
    E_OpCode opcode;
    uint32_t ack;
};

struct __attribute__((__packed__)) StartMsg
{
    SHeader header;
    uint32_t numOfPackets;
    char fileName[FILENAME_LENGTH];
};

struct __attribute__((__packed__)) EndMsg
{
    SHeader header;
};

union Message
{
    DataMsg data;
    AckMsg ack;
    EndMsg end;
    StartMsg start;
    uint8_t msg[sizeof(DataMsg)];
};