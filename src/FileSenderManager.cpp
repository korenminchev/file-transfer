#include "../include/FileSenderManager.h"


FileSenderManager::FileSenderManager() :  m_numOfReceivers(0),
    m_pipeFDS{}
{

}

FileSenderManager::~FileSenderManager()
{
    //Close Open Pipes
    for(uint8_t i = 0; i < m_numOfReceivers; i++)
    {
        if(m_pipeFDS[i] != 0)
        {
            close(m_pipeFDS[i]);
        }
    }
}


bool FileSenderManager::Init(char* argv[], int argc)
{
    //Check Input
    if(!ParseInput(argv, argc))
    {
        return false;
    }

    //Open File And Read It
    FILE* fileToSend = fopen(m_filePath, "r");
    if(fileToSend == nullptr)
    {
        printf("Error Opening File\n");
        return false;
    }

    //Get File Size To Calculate Precentages Sucessfully Sent
    fseek(fileToSend, 0L, SEEK_END);
    int64_t fileSize= ftell(fileToSend);
    //Check File Size
    if(fileSize > MAX_FILE_SIZE)
    {
        printf("File Is Too Large\n");
        return false;
    }

    //No Need Of The File Here Anymore
    fclose(fileToSend);

    m_fileChunks = fileSize/BUFFER_SIZE + 1;

    //Create Epoll
    int epollId = epoll_create(MAX_LISTENERS);
    if(epollId == ERROR)
    {
        printf("Error in Creating Epoll\n");
        return false;
    }

    m_epoll = epollId;

    //Create Child Processes And Pipes For Each One
    for(size_t i = 0; i < m_numOfReceivers; i++)
    {
        //For The Child Process To Know Which Sender Is He
        m_senderNumber = i;

        //Init Sender And Get Pipe For Talking To It
        m_pipeFDS[i] = m_fileSenders[i].Init(m_addresses[i], m_harmness, m_filePath);
        if (m_pipeFDS[i] == ERROR)
        {
            printf("Error Initializing File Sender %lu\n", i);
        }

        //Insert Pipe Into Epoll
        else if(InsertIntoEpoll(m_epoll, m_pipeFDS[i]))
        {
            //If Successfully Inserted Into Epoll, Create Child Process
            int32_t childFD = fork();
            //Close Pipe And Inform User That Process Creation Failed
            if(childFD == ERROR)
            {
                printf("Error Creating Process %lu\n", i);
                close(m_pipeFDS[i]);
                m_pipeFDS[i] = 0;
            }
            //If It's The Child Process, Exit The Init Loop
            else if(childFD == 0)
            {
                break;
            }
        }
        //If Failed To Instert To Epoll
        else
        {
            return false;
        }
        //If Manager
        m_senderNumber = -1;
    }
    return true;
}


bool FileSenderManager::ParseInput(char* argv[], int argc)
{
    if(argc < 6)
    {
        printf("Too Few Arguments Given\n");
        return false;
    }

    //Check That Harmness Level Is A Char
    if(strlen(argv[2]) > 1)
    {
        printf("Harmness Must Be A Char\n");
        return false;
    }
    char harm = argv[2][0];
    //Asign Harmness Level
    switch (harm)
    {
    case CORONET_HEALTHY:
        m_harmness = Coronet::Harmness::Healthy;
        break;
    case CORONET_INCUBATION:
        m_harmness = Coronet::Harmness::IncubationPeriod;
        break;
    case CORONET_MINOR:
        m_harmness = Coronet::Harmness::MinorSymptoms;
        break;
    case CORONET_SEVERE:
        m_harmness = Coronet::Harmness::SevereSymptoms;
        break;
    case CORONET_DYING:
        m_harmness = Coronet::Harmness::Dying;
        break;
    default:
        printf("Harmness Char Can Be %c,%c,%c,%c or %c\n", 
        CORONET_HEALTHY, CORONET_INCUBATION, CORONET_MINOR,
        CORONET_SEVERE, CORONET_DYING);
        return false;
    }

    //Check Path Traversal
    //(Files Are Only Allowed From The Program Dir Or Lower)
    char absuluteFilePath[FILENAME_LENGTH] = {'\0'};
    char programPath[FILENAME_LENGTH] = {'\0'};

    realpath(argv[3], absuluteFilePath);

    getcwd(programPath, FILENAME_LENGTH);

    if(strncmp(programPath, absuluteFilePath, strlen(programPath) != 0))
    {
        printf("File To Send Can't Be From A Dir. Above The Program\n");
        return false;
    }

    strncpy(m_filePath, absuluteFilePath, FILENAME_LENGTH);

    //Check If Given Max 10 Receivers
    if(argc > 24)
    {
        printf("Too Much Receivers Given (MAX 10)\n");
        return false;
    }

    if(argc % 2 != 0)
    {
        printf("Wrong Number Of Arguments Given\n");
        return false;
    }

    //Read Ip Addresses
    for(size_t i = 4; i < argc; i = i + 2)
    {
        //CheckIp
        if(inet_pton(AF_INET, argv[i], &m_addresses[(i-4)/2].sin_addr) != 1)
        {
            printf("%s\n", argv[i]);
            printf("%lu Receiver Address Incorrect\n", ((i-4)/2 + 1));
            return false;
        }
        //CheckPort
        uint16_t port = parsePort(argv[i + 1]);
        if(port == 0)
        {
            printf("%lu Receiver Port Incorrect\n", ((i-4)/2 + 1));
            return false;
        }
        m_addresses[(i-4)/2].sin_port = htons(port);
        m_addresses[(i-4)/2].sin_family = AF_INET;
        m_numOfReceivers++;
    }
    return true;
}


const void FileSenderManager::PrintProgress(uint8_t sender, uint32_t sent)
{
    //Go To The Line Of The Sender
    for(uint8_t i = 0; i < sender + 1; i++)
    {
        printf("\n");
    }
    //Calculate %
    uint32_t precentDone = (sent* 100)/m_fileChunks ;
    char progress[] = "[                    ]";
    //Fill Progress Bar
    for(uint8_t i = 1; i < (precentDone/5) + 1; i++)
    {
        progress[i] = '=';
    }
    //Print
    printf("\rSender %u: %s %u%% (%u/%u)", sender, progress, precentDone, sent, m_fileChunks);
    //Go Back To The First Line
    for(uint8_t i = 0; i < sender + 1; i++)
    {
        printf("\033[A");
    }
}


void FileSenderManager::Run()
{
    //If Not Main Proccess
    if(m_senderNumber != -1)
    {
        m_fileSenders[m_senderNumber].Run();
    }
    else
    {
        //Create Empty Events Array
        uint32_t senderProgress[MAX_LISTENERS]{};
        uint32_t packetsSuccSent;
        uint8_t finishedSenders = 0;
        bool allFinished = false;
        while(!allFinished)
        {
            //Get Events
            struct epoll_event pipeEvents[MAX_LISTENERS] = {};
            int events = epoll_wait(m_epoll, pipeEvents, MAX_LISTENERS*2, -1);
            //Handle
            for(size_t i = 0; i < events; i++)
            {
                int32_t currPipe = pipeEvents[i].data.fd;
                for(uint8_t j = 0; j < MAX_LISTENERS; j++)
                {
                    if(currPipe == m_pipeFDS[j])
                    //Listening To File Sender Number j
                    {
                        read(currPipe, &packetsSuccSent, sizeof(int32_t));
                        senderProgress[j] = packetsSuccSent;
                        
                        PrintProgress(j, packetsSuccSent);

                        //Check If The Current Sender Finished
                        if(senderProgress[j] >= m_fileChunks)
                        {
                            close(m_pipeFDS[j]);
                            finishedSenders++;
                            //Check If Everyone Finished
                            if(finishedSenders == m_numOfReceivers)
                            {
                                allFinished = true;
                            }
                        }
                    }
                }
            }
        }
        //Go To The Bottom Line
        for(uint i = 0; i < m_numOfReceivers+1; i++)
        {
            printf("\n");
        }
        printf("Done!\n");
        fflush(stdout);
    }
}