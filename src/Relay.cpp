#include "Relay.h"

Relay::Relay(): m_coronetReceiver(nullptr), m_coronetSender(nullptr)
{}

Relay::~Relay()
{
    if(m_coronetSender != nullptr)
    {
        delete m_coronetSender;
    }
    if(m_coronetReceiver != nullptr)
    {
        delete m_coronetReceiver;
    }
}


bool Relay::Init(int argc, char* argv[])
{
    //Parse Input
    if(!ParseInput(argc, argv))
    {
        return false;
    }

    //Init Adapters
    if(!m_adapterReceiver.Init(m_sourcePort))
    {
        printf("Error Initializing Network Adapter\n");
        return false;
    }

    if(!m_adapterSender.Init(m_destAddr))
    {
        printf("Error Initializing Network Adapter\n");
        return false;
    }

    //Create Network Instances
    m_coronetReceiver = new Coronet::Coronet(m_adapterReceiver, m_adapterReceiver, m_harmness);
    if(m_coronetReceiver == nullptr)
    {
        printf("Error Allocating Space For Network Instance\n");
        return false;
    }
    m_coronetSender = new Coronet::Coronet(m_adapterSender, m_adapterSender, m_harmness);
    if(m_coronetSender == nullptr)
    {
        printf("Error Allocating Space For Network Instance\n");
        return false;
    }

    return true;
}


bool Relay::ParseInput(int argc, char* argv[])
{
    //Check Number Of Arguments Given
    if(argc != RELAY_ARGS)
    {
        printf("Wrong Number Of Arguments\n");
        return false;
    }
    //Check That Harmness Level Is A Char
    if(strlen(argv[2]) > 1)
    {
        printf("Harmness Must Be A Char\n");
    }
    char harm = argv[2][0];
    //Asign Harmness Level
    switch (harm)
    {
    case CORONET_HEALTHY:
        m_harmness = Coronet::Harmness::Healthy;
        break;
    case CORONET_INCUBATION:
        m_harmness = Coronet::Harmness::IncubationPeriod;
        break;
    case CORONET_MINOR:
        m_harmness = Coronet::Harmness::MinorSymptoms;
        break;
    case CORONET_SEVERE:
        m_harmness = Coronet::Harmness::SevereSymptoms;
        break;
    case CORONET_DYING:
        m_harmness = Coronet::Harmness::Dying;
        break;
    default:
        printf("Harmness Char Can Be %c,%c,%c,%c or %c\n", 
        CORONET_HEALTHY, CORONET_INCUBATION, CORONET_MINOR,
        CORONET_SEVERE, CORONET_DYING);
        return false;
    }

    //Parse Sender Port
    m_sourcePort = parsePort(argv[3]);

    //Check If Valid
    if(m_sourcePort == 0)
    {
        return false;
    }

    //Check Receiver IP Addr
    if(inet_pton(AF_INET, argv[4], &m_destAddr.sin_addr) != 1)
    {
        printf("%s\n", argv[4]);
        printf("Destination Address Incorrect\n");
        return false;
    }

    //Check Receiver Port
    uint16_t destPort = parsePort(argv[5]);
    if(destPort == 0)
    {
        return false;
    }
    m_destAddr.sin_port = htons(destPort);
    m_destAddr.sin_family = AF_INET;

    return true;
}


const void Relay::Run()
{
    Message toPass;
    uint16_t count = 0;
    uint16_t timer = 0;
    //While Not TimeOut
    while(timer < RELAY_TIMEOUT)
    {
        //Inc Timer
        timer++;
        //While There Is Data To Read From The Sender, Read It And Resend To The Receiver
        while((m_coronetReceiver->Receive(toPass.msg, sizeof(Message))) > 0 && count < WINDOW_SIZE)
        {
            m_coronetSender->Send(toPass.msg, sizeof(Message));
            count++;
            timer = 0;
        }
        count = 0;
        timer++;
        //While There Is Data To Read From The Receiver, Read It And Resend To The Sender
        while((m_coronetSender->Receive(toPass.msg, sizeof(AckMsg))) > 0 && count < WINDOW_SIZE)
        {
            m_coronetReceiver->Send(toPass.msg, sizeof(AckMsg));
            count++;
            timer = 0;
        }
    }
    printf("Done!\n");
}