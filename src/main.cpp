#include "FileSenderManager.h"
#include "FileReceiver.h"
#include "Relay.h"

int main(int argc, char* argv[])
{
    if(argc < 2)
    {
        printf("Too Few Arguments\n");
        exit(1);
    }
    if(strncmp(argv[1], "send", sizeof("send")) == 0)
    {
        FileSenderManager sender;
        if(sender.Init(argv, argc))
        {
            sender.Run();
        }
    }
    else if(strncmp(argv[1], "receive", sizeof("receive")) == 0)
    {
        FileReceiver reciever;
        if(reciever.Init(argc, argv))
        {
            reciever.Run();
        }
    }
    else if(strncmp(argv[1], "relay", sizeof("relay")) == 0)
    {
        //printf("Relay\n");
        Relay relay;
        if(relay.Init(argc, argv))
        {
            //printf("Run\n");
            relay.Run();
        }
    }
}