#include "FileReceiver.h"

FileReceiver::FileReceiver() : m_done(0), m_file(nullptr), m_pipeFDS{}, 
    m_coronet(nullptr){}

FileReceiver::~FileReceiver()
{
    //Close Pipes
    if(m_pipeFDS[PIPE_READ] != 0)
    {
        close(m_pipeFDS[PIPE_READ]);
    }
    if(m_pipeFDS[PIPE_WRITE] != 0)
    {
        close(m_pipeFDS[PIPE_WRITE]);
    }
    if(m_coronet != nullptr)
    {
        delete m_coronet;
    }
}

bool FileReceiver::Init(int argc, char* argv[])
{
    if(argc != RECV_ARGS)
    {
        printf("Wrong Number Of Arguments\n");
        return false;
    }

    //Check That Harmness Level Is A Char
    if(strlen(argv[2]) > 1)
    {
        printf("Harmness Must Be A Char\n");
    }
    char harm = argv[2][0];
    //Asign Harmness Level
    switch (harm)
    {
    case CORONET_HEALTHY:
        m_harmness = Coronet::Harmness::Healthy;
        break;
    case CORONET_INCUBATION:
        m_harmness = Coronet::Harmness::IncubationPeriod;
        break;
    case CORONET_MINOR:
        m_harmness = Coronet::Harmness::MinorSymptoms;
        break;
    case CORONET_SEVERE:
        m_harmness = Coronet::Harmness::SevereSymptoms;
        break;
    case CORONET_DYING:
        m_harmness = Coronet::Harmness::Dying;
        break;
    default:
        printf("Harmness Char Can Be %c,%c,%c,%c or %c\n", 
        CORONET_HEALTHY, CORONET_INCUBATION, CORONET_MINOR,
        CORONET_SEVERE, CORONET_DYING);
        return false;
    }

    //Parse And Check Port
    m_port = parsePort(argv[3]);
    if(m_port == 0)
    {
        return false;
    }

    //Create A New File
    m_file = fopen("temp", "w");
    if(m_file == nullptr)
    {
        printf("Error Creating File\n");
        return false;
    }
    
    //Setup Network Adapter
    if(!m_adapter.Init(m_port))
    {
        printf("Error Initializing Network Adapter\n");
        return false;
    }

    //Setup Network
    m_coronet = new Coronet::Coronet(m_adapter, m_adapter, m_harmness);
    if(m_coronet == nullptr)
    {   
        printf("Error Creating Network Library\n");
        return false;
    }
 
    //Setup Pipes For Comunication Between Processes
    if(pipe(m_pipeFDS) == ERROR)
    {
        printf("Error Creating Pipes For Comunication\n");
        return false;
    }

    //Create Printing Process
    m_childPID = fork();
    if(m_childPID == ERROR)
    {
        printf("Error Creating Child Process\n");
        return false;
    }

    return true;
}


void FileReceiver::GetStart()
{
    Message start;

    //Wait For Ack
    ssize_t bytesReceived = m_coronet->Receive(start.msg, sizeof(StartMsg));
    bool okToStart = false;
    while(!okToStart)
    {
        //If No Data At Socket Read Again
        if(bytesReceived < static_cast<long>(sizeof(StartMsg)))
        {
            bytesReceived = m_coronet->Receive(start.msg, sizeof(StartMsg));
        }
        else
        {
            //Check Chksum
            uint32_t chkSum = start.start.header.checkSum;
            start.start.header.checkSum = 0;
            if(chkSum == crc32_16bytes(&start.msg, sizeof(StartMsg)))
            {
                //Copy FileName and fileChunks
                strncpy(m_fileName, start.start.fileName, FILENAME_LENGTH);
                m_fileChunks = start.start.numOfPackets;
                okToStart = true;
                //Send Ack To Sender
                SendAck(UINT32_MAX, OkStart);
            }
            //If Checksum Isn't Correct Wait For A New Packet
            else
            {
                bytesReceived = m_coronet->Receive(start.msg, sizeof(StartMsg));
            }
        }
    }
}


const void FileReceiver::SendAck(uint32_t seq, E_OpCode opCode)
{
    Message ack;
    ack.ack.ack = seq;
    ack.ack.opcode = opCode;
    ack.ack.header.type = Ack;
    ack.ack.header.checkSum = 0;
    ack.ack.header.checkSum = crc32_16bytes(&ack.msg, sizeof(AckMsg));
    m_coronet->Send(ack.msg, sizeof(AckMsg));
}


void FileReceiver::HandleMessage(Message& received)
{
    //If It's A Data Message
    if(received.data.header.type == Data)
    {
        //Get the seq number
        uint32_t seq = received.data.seq;
        //Handle Double Send
        if(m_readyIndex.test(seq))
        {
            SendAck(seq, Ok);
            return;
        }
        size_t listSize = m_readyToWrite.size();
        //If there are other messages in the list, sort the new one
        if(listSize > 0)
        {
            //Loop trough the list
            std::list<Message>::iterator it = m_readyToWrite.begin();
            for(size_t i = 0; i < listSize - 1; i++)
            {   
                //If the received seq num is the between the current and
                //previus message, place it there
                if(seq < (*it).data.seq)
                {
                    m_readyToWrite.insert(it, received);
                    m_readyIndex.set(seq);
                    SendAck(seq, Ok);
                    return;
                }
                it++;
            }
            //If we reached the end
            if(seq < (*it).data.seq)
            {
                m_readyToWrite.insert(it, received);
                m_readyIndex[seq] = true;
                SendAck(seq, Ok);
                return;
            }
            //If It's After The Last Packet
            m_readyToWrite.push_back(received);
            m_readyIndex.set(seq);
            SendAck(seq, Ok);
            return;
        }
        //If The List Is Empty
        else
        {
            m_readyToWrite.push_back(received);
            m_readyIndex.set(seq);
            SendAck(seq, Ok);
        }
    }
}


const void FileReceiver::ReceivePackets()
{
    Message received;
    //Get A Packet From The Network
    ssize_t bytesReceived = m_coronet->Receive(received.msg, sizeof(Message));
    //While There Is Still Data
    while(bytesReceived > 0)
    {
        //Check The Checksum
        uint32_t chkSum = received.data.header.checkSum;
        uint32_t chkSumStart = received.start.header.checkSum;
        received.data.header.checkSum = 0;
        if(chkSum == crc32_16bytes(&received, sizeof(DataMsg)) || 
                    chkSum == crc32_16bytes(&received, sizeof(StartMsg)))
        {
            //If The Sender Didn't Receive Start Ack
            if(received.start.header.type == Start)
            {
                SendAck(UINT32_MAX, OkStart);
            }
            //If It's A Data MSG
            else
            {
                HandleMessage(received);
            }
        }
        //Read From Network
        bytesReceived = m_coronet->Receive(received.msg, sizeof(Message));
    }
}

size_t FileReceiver::WriteToFile(uint32_t lastWritten)
{
    Message toWrite;
    //Check If There Is A Sequence In The Packets Write To File
    while(m_readyIndex.test(lastWritten))
    {
        toWrite = m_readyToWrite.front();
        fwrite((toWrite.data.data), toWrite.data.len, 1, m_file);
        //Releae Written Packet
        m_readyToWrite.pop_front();
        lastWritten++;
    }
    return lastWritten;
}


const void FileReceiver::PrintLoop()
{
    //Get The Number Of Processes From The Father
    read(m_pipeFDS[PIPE_READ], &m_fileChunks, sizeof(uint32_t));
    uint32_t written = 0;
    //While Not Finished
    while(written < m_fileChunks)
    {
        //Get The Number Of Successfully Written
        read(m_pipeFDS[PIPE_READ], &written, sizeof(uint32_t));
        //Calculate %
        uint32_t precentDone = (written* 100)/m_fileChunks ;
        char progress[] = "[                    ]";
        //Fill Progress Bar
        for(uint8_t i = 1; i < (precentDone/5) + 1; i++)
        {
            progress[i] = '=';
        }
        //Print
        printf("\rReceived: %s %u%% (%u/%u)", progress, precentDone, written, m_fileChunks);
    }
    printf("\nDone!\n");
}


void FileReceiver::Run()
{
    //Child Print Process
    if(m_childPID == 0)
    {
        PrintLoop();
    }
    else
    {
        //Get FileSize And FileName
        GetStart();
        //Write To Print Process The FileChunks
        write(m_pipeFDS[PIPE_WRITE], &m_fileChunks, sizeof(uint32_t));
        uint32_t lastWritten = 0;
        //Main Loop
        while(!m_done)
        {
            //Get Packets From Network
            ReceivePackets();
            //Write Them To The File
            lastWritten = WriteToFile(lastWritten);
            //Send To Print Process The Progres
            write(m_pipeFDS[PIPE_WRITE], &lastWritten, sizeof(uint32_t));
            //If We Wrote The Whole File
            if(lastWritten == m_fileChunks)
            {
                //Send End Ack To The Sender
                for(uint8_t i = 0; i < VALIDATE_SEND; i++)
                {
                    SendAck(UINT32_MAX, OkEnd);
                }
                fflush(m_file);
                m_done = true;
            }
        }
        //Rename The File
        fclose(m_file);
        int32_t ret = rename("temp", m_fileName);
    }
}