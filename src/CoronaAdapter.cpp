#include "../include/CoronaAdapter.h"

socklen_t ADDRESS_SIZE = sizeof(sockaddr_in);

CoronaAdapter::CoronaAdapter() : m_connectionAddress{}, m_socketFD(-1)
{

}

CoronaAdapter::~CoronaAdapter()
{
    if(m_socketFD != ERROR)
    {
        close(m_socketFD);
    }
}

bool CoronaAdapter::Init(sockaddr_in& address)
{
    //Create socket and check if successfull
    int32_t socketTemp;
    if((socketTemp = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        printf("Failed Creating Connection Socket\n");
        return false;
    }

    m_socketFD = socketTemp;

    //Send To Given Address
    memcpy(&m_connectionAddress, &address, ADDRESS_SIZE);

    //SetTimeout
    return SetTimeout(TIMEOUT);
}


bool CoronaAdapter::Init(uint16_t port)
{
    //Create socket and check if successfull
    int32_t socketTemp;
    if((socketTemp = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        printf("Failed Creating Connection Socket\n");
        return false;
    }

    m_socketFD = socketTemp;

    //Listen To Any Ip Address On Port port
    sockaddr_in bindAddress;
    bindAddress.sin_port = htons(port);
    bindAddress.sin_addr.s_addr = INADDR_ANY;
    bindAddress.sin_family = AF_INET;

    //Bind
    if(bind(m_socketFD, ((const sockaddr *)&bindAddress), ADDRESS_SIZE) < 0)
    {
        printf("Error Listening To Port\n");
        return false;
    }
    //Set Timeout
    return SetTimeout(1);
}


bool CoronaAdapter::SetTimeout(suseconds_t microSec)
{
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = microSec;
    if (setsockopt(m_socketFD, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) 
    {
        printf("Error Putting TimeOut On Socket\n");
        return false;
    }
    return true;
}


ssize_t CoronaAdapter::Send(const uint8_t* data, size_t dataSize)
{
    ssize_t bytesSent;
    bytesSent = sendto(m_socketFD, data, dataSize, MSG_CONFIRM,
                    (sockaddr*)&m_connectionAddress, ADDRESS_SIZE);
    return bytesSent;
}


ssize_t CoronaAdapter::Receive(uint8_t* o_buffer, size_t bufferSize)
{
    //Clear The Buffer
    memset(o_buffer, 0, bufferSize);

    sockaddr_in sourceAddr;
    ssize_t bytesRead;
    //Get Data From Network
    bytesRead = recvfrom(m_socketFD, o_buffer, bufferSize, MSG_WAITALL,
                (sockaddr*) &sourceAddr, &ADDRESS_SIZE);
    //TimeOut
    if(bytesRead == ERROR)
    {
        if(errno == EAGAIN || errno == EWOULDBLOCK)
        {
            return SOCK_TIMEOUT;
        }
        printf("%s\n", strerror(errno));
        return ERROR;
    }
    //If It's The First Connection
    if(m_connectionAddress.sin_port == 0)
    {
        memcpy(&m_connectionAddress, &sourceAddr, ADDRESS_SIZE);
    }
    //If Connection Is Not From First Sender
    else if(memcmp(&m_connectionAddress, &sourceAddr, ADDRESS_SIZE) != 0)
    {
        return BAD_ADDR;
    }
    return bytesRead;
}